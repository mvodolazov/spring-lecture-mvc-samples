package com.nixsolutions.mvc.example.controller;

import com.nixsolutions.mvc.example.dto.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentController {
    @RequestMapping(value="student", method = RequestMethod.GET)
    public ModelAndView country(){
        return new ModelAndView("studentForm","student",new Student());
    }

    @RequestMapping(value="saveStudent", method = RequestMethod.POST)
    public String saveStudent(@ModelAttribute("student") Student student, Model model) {
        model.addAttribute("name", student.getName());
        model.addAttribute("age", student.getAge());
        return "viewStudent";
    }
}
