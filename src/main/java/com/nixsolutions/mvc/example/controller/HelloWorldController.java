package com.nixsolutions.mvc.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/greeting")
public class HelloWorldController  {

	@RequestMapping(value="/hello", method = RequestMethod.GET)
	private String hello(Model model) throws Exception {
		model.addAttribute("msg", "hello world");
		return "helloWorld";
	}

}