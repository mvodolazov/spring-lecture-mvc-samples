<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Student Info</title>
</head>
<body>
    <table>
        <form:form action="saveStudent" method="post" commandName="student">
            <tr>  <td>Student Name:</td> <td><form:input  path="name"/> </td> </tr>
            <tr> <td> Student Age :</td> <td><form:input path="age"/> </td> </tr>
            <tr> <td colspan=2>   <input type="submit"> </td>
        </form:form>
    </table>
</body>
</html>
